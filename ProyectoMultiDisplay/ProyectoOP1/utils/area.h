//
// Created by emmanuel on 20/04/19.
//

#ifndef MULTIDISPLAY_AREA_H
#define MULTIDISPLAY_AREA_H

typedef struct area{
    int width;
    int height;
} area;

#endif //MULTIDISPLAY_AREA_H
